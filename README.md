[![Netlify Status](https://api.netlify.com/api/v1/badges/fd174668-7c36-4160-829e-3fd77f2d3d82/deploy-status)](https://app.netlify.com/sites/luis-agm-com-v2/deploys)

# Personal website

Small presentational website migrated from Vue 2 to Vue 3 and using Vite build tool.